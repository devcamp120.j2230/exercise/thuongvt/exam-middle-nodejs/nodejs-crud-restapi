//Khai báo thư viện mongose
const mongoose = require("mongoose");
//Khai báo thư viện model
const carModel = require("../model/car.model");
const userModel = require("../model/user.model");

// creatCarOfUser
const creatCarOfUser = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.userId
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    if (!body.model) {
        return res.status(400).json({
            message: " Model is not valid"
        })
    }
    if (!body.vId) {
        return res.status(400).json({
            message: " vId is not valid"
        })
    }
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    let newUserData = {
        _id: mongoose.Types.ObjectId(),
        model: body.model,
        vId: body.vId
    }
    carModel.create(newUserData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        userModel.findByIdAndUpdate(Id, { $push: { cars: data._id } }, (err1, data1) => {
            if (err1) {
                return res.status(200).json({
                    status: "Internal server error1",
                    message: err1.message
                })
            }
            return res.status(201).json({
                status: "Create cars successfully",
                data: data1
            })
        })
    })
}
// createCar, 
const createCar = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!body.model) {
        return res.status(400).json({
            message: " Model is not valid"
        })
    }
    if (!body.vId) {
        return res.status(400).json({
            message: " vId is not valid"
        })
    }
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    let newCarData = {
        _id: mongoose.Types.ObjectId(),
        model: body.model,
        vId: body.vId
    }
    carModel.create(newCarData, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(201).json({
            message: "Tạo dữ liệu car thành công",
            Data: data
        })
    })
}
// getAllCar, 
const getAllCar = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Kiểm tra dữ liệu
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    carModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(201).json({
            message: "lấy toàn bộ dữ liệu car thành công",
            Data: data
        })
    })
}
// getCarById, 
const getCarById = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.carId
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    carModel.findById(Id, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "lấy car thành công thông qua Id",
            Data: data
        })
    })
}
// UpdateCarById, 
const updateUserById = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.carId
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    if (!body.model) {
        return res.status(400).json({
            message: " Model is not valid"
        })
    }
    if (!body.vId) {
        return res.status(400).json({
            message: " vId is not valid"
        })
    }
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    let newCarDataUpdate = {
        model: body.model,
        vId: body.vId
    }
    carModel.findByIdAndUpdate(Id, newCarDataUpdate, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Update car thành công thông qua Id",
            Data: data
        })
    })
}
// deleteCarById, 
const deleteCarById = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.carId
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    carModel.findByIdAndDelete(Id, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message: "Delete car thành công thông qua Id",
            Data: data
        })
    })
}
// getAllCarOfUser
const getAllCarOfUser = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.userId
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    userModel
        .findById(Id)
        .populate("cars")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(200).json({
                status: "Get car successfully",
                Data: data.cars
            })
        })
}
//exprot thành modul
module.exports = {
    createCar,
    getAllCar,
    getCarById,
    updateUserById,
    deleteCarById,
    getAllCarOfUser,
    creatCarOfUser
}

