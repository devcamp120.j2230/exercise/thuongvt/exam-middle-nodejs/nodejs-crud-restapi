//Khai báo thư viện mongose
const mongoose = require("mongoose");
//Khai báo thư viện model
const userModel = require("../model/user.model");
// createUser
const createUser = (req, res) => {
    //B1 Chuẩn bị dữ liệu
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!body.name) {
        return res.status(400).json({
            message: "name is not valid!"
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: "phone is not valid!"
        })
    }
    if (!body.age) {
        return res.status(400).json({
            message: "age is not valid!"
        })
    }
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    let newUserData = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        phone: body.phone,
        age:body.age
    }
    userModel.create(newUserData,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(201).json({
            message:"Tạo dữ liệu user thành công",
            Data: data
        })
    })
}
// getAllUser
const getAllUser =(req,res)=>{
    //B1 Chuẩn bị dữ liệu
    //B2 Kiểm tra dữ liệu
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    userModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"Lấy toàn bộ dữ liệu user thành công",
            Data: data
        })
    })

}
// getUserById
const getUserById =(req,res)=>{
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.userId
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    userModel.findById(Id,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"lấy user thành công thông qua Id",
            Data: data
        })
    })
}
// updateUserById
const updateUserById =(req,res)=>{
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.userId
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    if (!body.name) {
        return res.status(400).json({
            message: "name is not valid!"
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: "phone is not valid!"
        })
    }
    if (!body.age) {
        return res.status(400).json({
            message: "age is not valid!"
        })
    }
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    let newUserData = {
        name: body.name,
        phone: body.phone,
        age:body.age
    }
    userModel.findByIdAndUpdate(Id,newUserData,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"Update user thành công thông qua Id",
            Data: data
        })
    })
}
// deleteUserById
const deleteUserById = (req,res)=>{
    //B1 Chuẩn bị dữ liệu
    let Id = req.params.userId
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    //B3 Gọi model thực hiện bào toán tạo mới dữ liệu
    userModel.findByIdAndDelete(Id,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        return res.status(200).json({
            message:"Delete user thành công thông qua Id",
            Data: data
        })
    })
}
// exprots thành modul
module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}