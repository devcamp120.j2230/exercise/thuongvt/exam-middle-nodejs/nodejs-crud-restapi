//Improt thư viện mongoose
const mongoose = require("mongoose");
//Khai báo classs schema của thư viện
const schema = mongoose.Schema;
//Khai báo carschema 
const carschema = new schema({
    //Trường Id 
    _id:{
        type: mongoose.Types.ObjectId,
        require: true
    },
    //Trường model
    model:{
        type: String,
        require: true
    },
    // Trường vId
    vId:{
        type: String,
        require: true,
        unique: true
    }
},{timestamps:true});

//exprot thành modul
module.exports=mongoose.model("Car",carschema);