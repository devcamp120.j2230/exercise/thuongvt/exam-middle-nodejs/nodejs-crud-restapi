//Improt thư viện mongoose
const mongoose = require("mongoose");
//Khai báo classs schema của thư viện
const schema = mongoose.Schema;
//Khai báo userschema 
const userschema = new schema({
    //trường Id
    _id:{
        type: mongoose.Types.ObjectId,
        require:true
    },
    //trường name
    name:{
        type: String,
        require:true
    },
    //trường phone
    phone:{
        type: String,
        require:true,
        unique: true
    },
    //trường age
    age:{
        type: Number,
        default:0
    },
    //trường cars
    cars:[
        {
            type: mongoose.Types.ObjectId,
            ref:"Car"
        }
    ]
},{timestamps:true})

//exprot thành modul
module.exports=mongoose.model("User",userschema);

