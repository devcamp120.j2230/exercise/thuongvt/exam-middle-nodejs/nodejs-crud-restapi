//Khai báo thư viện express
const express = require("express")
//Khai báo Middleware 
const carMiddleware = require("../middleware/car.middleware")
//khai báo controller
const carController = require("../controller/car.controller")
//Khai báo router
const carRoute = express.Router();
 carRoute.post("/cars",carMiddleware.Middleware,carController.createCar);
 carRoute.get("/cars",carMiddleware.Middleware,carController.getAllCar);
 carRoute.get("/cars/:carId",carMiddleware.Middleware,carController.getCarById);
 carRoute.put("/cars/:carId",carMiddleware.Middleware,carController.updateUserById);
 carRoute.delete("/cars/:carId",carMiddleware.Middleware,carController.deleteCarById);
 carRoute.get("/users/:userId/car",carMiddleware.Middleware,carController.getAllCarOfUser);
 carRoute.post("/users/:userId/car",carMiddleware.Middleware,carController.creatCarOfUser)

 //exprot thành module
 module.exports = {carRoute}