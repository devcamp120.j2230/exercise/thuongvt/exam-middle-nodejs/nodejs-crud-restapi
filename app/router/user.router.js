//Khai báo thư viện express
const express = require("express")
//Khai báo Middleware 
const userMiddleware = require("../middleware/user.middleware");
//khai báo controller
const userController = require("../controller/user.controller")
//Khai báo router
const userRouter = express.Router();
//sử dụng router
userRouter.post("/users",userMiddleware.Middleware,userController.createUser);
userRouter.get("/users",userMiddleware.Middleware,userController.getAllUser);
userRouter.get("/users/:userId",userMiddleware.Middleware,userController.getUserById);
userRouter.put("/users/:userId",userMiddleware.Middleware,userController.updateUserById);
userRouter.delete("/users/:userId",userMiddleware.Middleware,userController.deleteUserById)
//exprot thành module
module.exports = {userRouter}