// khai báo thư viện express
const express = require("express");
const app = express();
const port = 8000;


// import thư viện mogoose
const mongoose = require("mongoose");
//import các router
const { carRoute } = require("./app/router/car.router");
const { userRouter } = require("./app/router/user.router");

//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))
mongoose.set('strictQuery', true);

// Kết nối với mongodb
mongoose.connect(`mongodb://localhost:27017/NodeJS-CRUD-RestAPI`, function(error) {
    if (error) throw error;
    console.log('Kết nối thành công với mongoDB');
   })

// nơi sử dụng các router
app.use(userRouter);
app.use(carRoute)
app.listen(port, () => {
    console.log(`App đã chạy trên cổng : ${port}`);
})